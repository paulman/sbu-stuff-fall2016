import sys
import re

class Passenger:
    def __init__(self):
        self.start = 0
        self.end = 0

cost_map = {0:0, 1:1, 2:1.9, 3:2.7, 4:3.4, 5:4, 6:4.5, 7:4.9, 8:5.2, 9:5.4, 10:5.5}

pass_list = []
max = 0
filename = sys.argv[1]

with open(filename) as f:
    for line in f:
        if line.startswith("passenger("):
            pass_info = (re.findall(r'\((.*?)\)', line))[0]
            pass_info_list = [str.strip() for str in pass_info.split(',')]
            p = Passenger()
            p.start = int(pass_info_list[1])
            p.end = int(pass_info_list[2])
            if p.end > max:
                max = p.end
            pass_list.append([int(pass_info_list[1]),int(pass_info_list[2])])

people = len(pass_list)
stations = max
full_price = 0

pass_start_queue = {}
pass_end_queue = {}

start_station_list = []
end_station_list = []

pass_list = list(pass_list)

for i in range(people):
    start_station_list.append(int(pass_list[i][0]))
    end_station_list.append(int(pass_list[i][1]))
for i in range(people):
    if start_station_list[i] not in pass_start_queue:
        pass_start_queue[start_station_list[i]] = 0
    pass_start_queue[start_station_list[i]] += 1
    if end_station_list[i] not in pass_end_queue:
        pass_end_queue[end_station_list[i]] = 0
    pass_end_queue[end_station_list[i]] += 1

    travel_dist = end_station_list[i] - start_station_list[i]

    if travel_dist > 10:
        travel_dist = 10
    full_price += cost_map[travel_dist]

start_station_list = sorted(list(set(start_station_list)))
end_station_list = sorted(list(set(end_station_list)))
swap_path = []
swap_price = 0
while len(start_station_list) > 0 or len(end_station_list) > 0:
    if len(start_station_list) == 0 or end_station_list[0] < start_station_list[0]:
        station_ptr = end_station_list.pop(0)
        swap_path_ptr = pass_end_queue[station_ptr]
        while swap_path_ptr > 0:
            if (swap_path[-1][1] < swap_path_ptr):
                dist = swap_path[-1][1]
            else:
                dist = x
                swap_path_ptr -= dist
            swap_path[-1][1] -= dist
            temp_start = swap_path[-1][0]
            travel_dist = station_ptr - temp_start
            if travel_dist > 10:
                travel_dist = 10
            swap_price += cost_map[travel_dist]
            if swap_path[-1][1] == 0:
                swap_path.pop()
    else:
        station_ptr = start_station_list.pop(0)
        swap_path.append([station_ptr, pass_start_queue[station_ptr]])

loss = round((full_price - swap_price), 2)
print("loss(%.1f)." % loss)