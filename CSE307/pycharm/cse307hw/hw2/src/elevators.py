import sys
import re
import collections

class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = collections.defaultdict(list)

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node):
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)

def get_edge_cost(time, start, end):
    weight = 2*(abs(start - end))
    if time == 0:
        return abs(start - end)
    if time == weight:
        return abs(end - start)
    if time > weight:
        cost = weight - (time % weight)
        if time % weight == 0:
            return int(cost / 2)
        return int(cost + (weight/2))
    if time < weight:
        return int(weight - time + weight/2)


def find_path(graph, start, end, time, path=[]):
    path = path + [start]
    if start == end:
        return time
    if start not in graph.edges:
        return None
    shortest = None
    for node in graph.edges[start]:
        if node not in path:
            cost = time + get_edge_cost(time, start, node)
            new_weight = find_path(graph, node, end, cost, path)
            if new_weight:
                if not shortest or new_weight < shortest:
                    shortest = new_weight

    return shortest

graph = Graph()
filename = sys.argv[1]
top = 0
elevators = 0

with open(filename) as f:
    for line in f:
        if line.startswith("top"):
            top = (re.findall(r'\d+', line))[0]
        if (line.startswith("elevators")):
            elevators = (re.findall(r'\d+', line))[0]
        if (line.startswith("elevator(")):
            edge = re.findall(r'\d+', line)
            from_node = int(edge[1])
            to_node = int(edge[2])
            if from_node not in graph.nodes:
                graph.add_node(from_node)
            if to_node not in graph.nodes:
                graph.add_node(to_node)
            graph.add_edge(from_node, to_node)

print("min_time(%d)." % find_path(graph, 0, int(top), 0))