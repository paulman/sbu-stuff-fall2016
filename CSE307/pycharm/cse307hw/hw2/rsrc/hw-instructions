

Due: Friday, September 30, 11:59pm

Implement in python the following problems. Each program should be submitted in a single file (see the names below) and will be tested with



     python fileName.py inputFileName.txt



The input files contain one fact per line like the examples below.



1. Elevators (File name: elevators.py) (10 points)

The Empire State Building has N elevators. Each elevator is connecting exactly two floors and it does not stop on the floors between that two floors. The speed of all the elevators is the same: 1 second to pass one floor.

In the beginning, each elevator is in its lower position and they are starting cruising to the upper floor. After some elevator come to its upper position, it immediately starts to go back to its lower position, and so on.

Peter is on the first (the lowest) floor 0 and he wants to go to the top floor as soon as possible. He can change elevators only on the floors that are common to both elevators. The elevators are constantly moving, so Peter
has to wait sometimes for the next elevator. If the other elevator is in that moment on that floor, that change does not take any time. The goal is to find out the quickest possible time that Peter can reach the top floor.

Input Format

An input file for contains the following facts:

    One fact of the form top(K), which specifies the highest floor, and goal,
    One fact of the form elevators(N), which specifies the number of elevators,
    For each I in 1..N, there is a fact of the form elevator(I,B,T), where I is the elevator number, B is the starting floor of the elevator, and T is the top floor of the elevator,

The input data guarantees that a solution exists.

Output format

The output should contain exactly one fact of the form time(S), where S is the minimum amount of time Peter can make it to the top floor.

Examples

Input
top(10).
elevators(4).
elevator(1,0,5).
elevator(2,5,10).
elevator(3,5,7).
elevator(4,7,10).

Output
time(15).
-----------------------
Input
top(10).
elevators(4).
elevator(1,0,5).
elevator(2,5,10).
elevator(3,5,8).
elevator(4,8,10).

Output
time(14).
-----------------------


How?

Use Case 1: Peter takes the elevator 1 from floor 0 to floor 5. Here, he has 2 options:

Case 1.1: Peter waits 5 seconds for elevator 2 to come down back to 5 (because by the time Peter gets to floor 5, the elevator 2 is up to floor 10). He takes the elevator 2 and gets to floor 10 at second 15.

Case 1.2: Peter waits 3 seconds for elevator 3 to come down back to 5 (because by the time Peter gets to floor 5, the elevator 3 is at floor 6 going up). He takes the elevator 3 and gets to floor 7 at second 10.
The elevator 4 is at floor 9 going down, so Peter has to wait 2 seconds to take it. Peter gets to floor 10 at second 15.


Use Case 2: Peter takes the elevator 1 from floor 0 to floor 5. Here, he has 2 options:

Case 2.1: Peter waits 5 seconds for elevator 2 to come down back to 5 (because by the time Peter gets to floor 5, the elevator 2 is up to floor 10). He takes the elevator 2 and gets to floor 10 at second 15.

Case 2.2: Peter waits 1 second for elevator 3 to come down back to 5 (because by the time Peter gets to floor 5, the elevator 3 is at floor 6 going down). He takes the elevator 3 and gets to floor 8 at second 8.
Elevator 4 is at floor 9 going up, so Peter has to wait 3 seconds. Peter gets to floor 10 at second 14.

Note: Peter can also choose to go down with some elevators, like in this case:

elevator(1,1,3).

elevator(2,2,3).

elevator(3,2,10).

In this case, Peter will go from floor 1 to floor 3, then from floor 3 to floor 2, and then from floor 2 to floor 10.



2. Ticket Swapping (File name: ticketswap.py) (10 points)

The Metropolitan Transportation Authority is consulting with you regarding a new pricing scheme for New York City's subway system. In this new system, each passenger receives a ticket when they enter the subway, which indicates the station they entered at. When a passenger leaves the subway, they surrender their ticket and are charged a fee based on the distance between their origin and the current station.

The passenger is charged $1 for traveling a distance of 1 station. For every additional station, they are given a 10% discount. For example, traveling between 2 stations costs $1.90, traveling between 3 stations costs $2.70, and so on. Following this pattern, after traveling a distance of 10 stations, every additional station is free of charge.

After introducing this system, the MTA realized they were not seeing the profits they expected. Thorough investigation revealed that passengers were swapping their tickets after boarding the subway, essentially splitting the profits amongst themselves by maximizing their discounts. MTA would like you to develop a program that returns the maximum amount of profit that can be lost by this ticket swapping crisis.

We will consider only one subway and only one direction (from station 1 to station N, passing through all the stations in order) of the subway. We assume a passenger travelling from station A to B obtaining an entry card at A, can swap the entry card any number of times with any other passengers anywhere between A and B, including swapping with people who leave at A or those who enter at B, and then exit the train at B with some entry card (it is necessary to surrender some entry card to exit the subway). We also assume the passenger will not exit the train in the meantime (that is, will not surrender the currently held card and obtain a new one).

Input Format

An input file for contains the following facts:

    For each passenger P, there is a fact of the form passenger(P,A,B), where P is the name of the passenger, A is the entry station and B is the exit station (with A<B).

Output format

The output should contain exactly one fact of the form loss(S), where S is the maximum amount of profit that can be lost by ticket swapping.

Example

Input
passenger(paul, 1, 3).
passenger(john, 3, 6).

Output
loss(0.6).
-----------------------
How?

If Paul pays the price from station 1 to 3, then he pays $1+$0.9 = $1.9

John will pay:  $1+$0.9 + $0.8 = $2.7

Total: $4.6

However, if Paul swaps tickets with John, then Paul pays $0 (because he allegedly entered at station 3 and exited at station 3), while John pays  $1+$0.9 + $0.8 + $0.7 + $0.6 = $4, and MTA looses 60c.



Total points: 20 points.
