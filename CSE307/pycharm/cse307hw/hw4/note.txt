'''block  : LBRACE statement_list RBRACE'''

'''statement_list: statement statement_list
		        | statement'''

'''statement : NAME EQUALS expression '''

'''statement: IF LPAREN expression RPAREN block '''

'''statement : expression'''

'''expression : expression PLUS expression
	          | expression MINUS expression
	          | expression TIMES expression
 	          | expression MODULO expression
              | expression EXPONENT expression
              | expression FLOOR expression
              | expression LT expression
              | expression LTE expression
              | expression GT expression
              | expression GTE expression
              | expression EE expression
              | expression NOTEQUAL expression
              | expression DIVIDE expression
              | expression AND expression
              | expression OR expression
              | expression NOT expression '''

'expression : MINUS expression %prec UMINUS'
'expression : LPAREN expression RPAREN'
'expression : NOT expression'

'''expression : NUMBER
              | NUMBER COMMA'''

'''expression : STRING
              | LBRACK STRING RBRACK
              | STRING COMMA
              | STRING LBRACK NUMBER RBRACK'''

'''expression : REAL
              | REAL COMMA'''

'expression : NAME'

'''list : LBRACK expressions RBRACK
        | LBRACK expressions RBRACK LBRACK NUMBER RBRACK'''

'expressions : expression expressions'
'expressions : empty'

'empty :'
'expression : list'




