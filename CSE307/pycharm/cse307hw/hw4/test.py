reserved = ( 'PRINT','IF')

tokens = reserved + (
    'NAME' ,'INTEGER', 'REAL', 'STRING', 'IN', 'LT', 'LTE', 'GT', 'GTE', 'AND', 'OR', 'NOT', 'EQ', 'NEQ',
    'PLUS' ,'MINUS' ,'TIMES' ,'DIVIDE' , 'MOD', 'EXPO', 'FLOOR' , 'EQUALS',
    'LPAREN' ,'RPAREN', 'LBRACK' , 'RBRACK' , 'LIST', 'INDEX', 'COMMA', 'LBRACE', 'RBRACE', 'SEMI',

)


# ind r'\[(\d+)\]'
#list r'\[((\d+),)*(\d+)\]'
#empty, single, full list: r'\[(\s*\d+\s*,?)*\]'
#list with index: \[(\s*\d+\s*,?)*\]\[(\d+)\]

# Math Tokens
t_PLUS    = r'\+'
t_MINUS   = r'-'
t_TIMES   = r'\*'
t_DIVIDE  = r'/'
t_MOD     = r'%'
t_EXPO    = r'\*\*'
t_FLOOR   = r'\/\/'
t_EQUALS  = r'='

#Compare Tokens
t_LT      = r'<'
t_LTE     = r'<='
t_GT      = r'>'
t_GTE     = r'>='
t_EQ      = r'=='
t_NEQ     = r'<>'

# Literal Tokens
t_LPAREN  = r'\('
t_RPAREN  = r'\)'
t_LBRACK  = r'\['
t_RBRACK  = r'\]'
t_COMMA   = r','
t_LBRACE = r'{'
t_RBRACE = r'}'
t_SEMI    = r';'
t_INDEX   = r'\[(\d+)\]'
t_NAME    = r'[a-zA-Z_][a-zA-Z0-9_]*'



#to map the reserved words to the lower case counterparts in your program
reserved_map = {}
for r in reserved:
    print(r)
    reserved_map[r.lower()] = r


#Boolean Tokens
def t_AND(t):
    r'and'
    return t

def t_IN(t):
    r'in'
    return t

def t_OR(t):
    r'or'
    return t

def t_NOT(t):
    r'not'
    return t

def t_REAL(t):
    r'-?\d+\.\d*'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Real value too large %f", t.value)
        t.value = 0
    return t

def t_INTEGER(t):
    r'-?\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_STRING(t):
    r'\"[^\"]*\"'
    print("STRIIIIIING")
    t.value = t.value[1:-1]
    return t

# Ignored characters
t_ignore = " \t\n"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
import ply.lex as lex
lex.lex()

# Parsing rules

precedence = (
    ('left' ,'PLUS' ,'MINUS', 'IN' , 'LT', 'LTE', 'EQ', 'NEQ', 'GT', 'GTE', 'NOT' , 'AND' , 'OR'),
    ('left' ,'TIMES' ,'DIVIDE' , 'MOD' , 'EXPO' , 'FLOOR'),
    ('right' ,'UMINUS'),
)

# dictionary of names
names = { }

# ######  CODE BLOCK  ######
def p_block(t):
    '''block  : LBRACE statement_list RBRACE'''
    print("BLOOOOOOOOCK")
    t[0] = BlockNode(t[2])
# ###########################

#STATEMENTS
# def p_statement_line(t):
#     'statement : statement SEMI'
#     return t;

def p_statement_assign(t):
    'statement : NAME EQUALS expression'
    names[t[1]] = t[3]

def p_statement_if(t):
    '''statement : IF LPAREN expression RPAREN block '''
    print("IF")

def p_statement_expr(t):
    'statement : expression'
    print(t[1])

def p_statement_list(t):
    '''statement_list : statement statement_list
		              | statement'''
    t[0] = [t[1]] + t[2]



def p_statement_print(p):
    ''' statement : PRINT  expression  SEMI '''
    print("YOOOO")
    p[0] = PrintNode(p[3])  # create nodes in the tree instead of executing the current expression
# def p_statements_empty(t):
#     'statements : empty'
#     t[0] = []
###########

#EXPRESSIONS
def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = -t[2]

def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]

def p_expression_integer(t):
    '''expression : INTEGER
                  | INTEGER COMMA'''
    t[0] = t[1]

def p_expression_real(t):
    '''expression : REAL
                  | REAL COMMA'''
    t[0] = t[1]

def p_expression_string(t):
    '''expression : STRING
                  | STRING COMMA'''
    t[0] = t[1]

def p_expresion_name(t):
    'expression : NAME'
    print("naaaaaaame", t[1])

def p_expression_empty(t):
    'expression : empty'
    pass
#you wouldnt have the case below where its an uninitalized variable x; so id take this out to make it easier
# def p_expression_name(t):
#     'expression : NAME SEMI'
#     try:
#         t[0] = names[t[1]]
#     except LookupError:
#         print("Undefined name '%s'" % t[1])
#         t[0] = 0

###### ARRAYS #######

def p_list(t):
    'list : LBRACK expressions RBRACK'
    t[0] = t[2]
def p_expressions_expression(t):
    '''expressions : expression expressions'''
    t[0] = [t[1]] + t[2]
def p_expressions_empty(t):
    'expressions : empty'
    t[0] = []
def p_empty(t):
    'empty :'
    pass
def p_expression_list(t):
    'expression : list'
    t[0] = t[1]
def p_expression_elem(t):
    'expression : list INDEX'
    t[0] = t[1][int(t[2][1:-1])]

def p_expression_char(t):
    'expression : STRING LBRACK INTEGER RBRACK'
    t[0] = t[1][t[3]]
#####################



def bool_and(left, right):
    if left and right: return 1
    return 0

def bool_or(left, right):
    if left or right: return 1
    return 0

def bool_not(term):
    return int(not(bool_and(term, 1)))

def bool_in(left, right):
    return left in right

##### OPERATIONS #####
def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression MOD expression
                  | expression EXPO expression
                  | expression FLOOR expression'''
    try:
        if t[2] == '+'   : t[0] = t[1] + t[3]
        elif t[2] == '-' : t[0] = t[1] - t[3]
        elif t[2] == '*' : t[0] = t[1] * t[3]
        elif t[2] == '/' : t[0] = t[1] / t[3]
        elif t[2] == '%' : t[0] = t[1] % t[3]
        elif t[2] == '**': t[0] =  t[1] ** t[3]
        elif t[2] == '//': t[0] = int(t[1] / t[3])
    except:
        print("error")
       # p_sem_error(t)

def p_expression_cmpop(t):
    '''expression : expression LT expression
                  | expression GT expression
                  | expression LTE expression
                  | expression GTE expression
                  | expression EQ expression
                  | expression NEQ expression'''
    if t[2] == '<' : t[0] = int(t[1] < t[3])
    elif t[2] == '>': t[0] = int(t[1] > t[3])
    elif t[2] == '<=': t[0] = int(t[1] <= t[3])
    elif t[2] == '>=': t[0] = int(t[1] >= t[3])
    elif t[2] == '==': t[0] = int(t[1] == t[3])
    elif t[2] == '<>': t[0] = int(t[1] != t[3])

def p_expression_boolop(t):
    '''expression : expression AND expression
                  | expression OR expression
                  | expression IN expression
                  | NOT expression'''
    if t[2] == 'and': t[0] = bool_and(t[1], t[3])
    elif t[2] == 'or': t[0] = bool_or(t[1], t[3])
    elif t[1] == 'not': t[0] = bool_not(t[2])
    elif t[2] == 'in': t[0] = bool_in(t[1], t[3])
##################################

#######  ERROR  ######
def p_error(t):
    print("type: ", t, " value: ", t.value)
    print("SYNTAX ERROR")
    # while True:
    #     tok = lex.token()
    #     if not tok: break

# def p_sem_error(t):
#     t = ""
#     print("SEMANTIC ERROR")
#     while True:
#         tok = lex.token()
#         if not tok: break
################################

class Node:
    def __init__(self):
        print("Node")

    def evaluate(self):
        print("Evaluate")
        return 0

    def execute(self):
        print("Execute")

class NumberNode(Node):
    def __init__(self, v):
        self.value = int(v)
        print("NumberNode")

    def evaluate(self):
        print("Evaluate NumberNode")
        return self.value

    def execute(self):
        print("Execute NumberNode")

class StringNode(Node):
    def __init__(self, v):
        self.value = str(v)
        self.value = self.value[1:-1]  # to eliminate the left and right double quotes
        print("StringNode")

    def evaluate(self):
        print("Evaluate StringNode")
        return self.value

    def execute(self):
        print("Execute StringNode")

class PrintNode(Node):
    def __init__(self, v):
        self.value = v

    def evaluate(self):
        return 0

    def execute(self):
        if (isinstance(self.value,str) or isinstance(self.value,int) or isinstance(self.value,float) ):
            print(self.value)
        else:
            print(self.value.evaluate())

class IfNode(Node):
    def __init__(self, c, t, e):
        self.condition = c
        self.thenBlock = t
        self.elseBlock = e
        print("IfNode")

    def evaluate(self):
        print("Evaluate IfNode")
        return 0

    def execute(self):
        print("Execute IfNode")
        if (self.condition.evaluate()):
            self.thenBlock.execute()
        else:
            self.elseBlock.execute()


class BlockNode(Node):
    def __init__(self, l):
        # print("hi")
        self.statements = l

    def execute(self):
        # sl = self.l.evaluate()
        print("hello")
        for statement in self.statements:
            statement.execute()


# sample parse rules:
# def p_statement_print(p):
#     ''' statement : PRINT LPAREN expression RPAREN SEMI '''
#     print("YOOOO")
#     p[0] = PrintNode(p[3])  # create nodes in the tree instead of executing the current expression

# def p_statement_block(p):
#     ''' statement : LBRACE statements RBRACE'''
#     print("BLOCK")
#     p[0] = BlockNode(p[2])

import ply.yacc as yacc
import sys
# parser = yacc.yacc()
# filename = sys.argv[1]
#
# # execute the abstract syntax tree for the whole program that you read from the file
# try:
#     with open(filename, 'r') as file:
#         content = file.read()
# except EOFError:
#         print("Uh-oh")
# result = parser.parse(content)
# #print(result)
#
# # with open(filename, 'r') as file:
# #     content = file.read()
# # ast = parser.parse(content)
# # print(type(content))
# # ast.execute()

# I changed it so the entire file is read into 1 string so you can call execute on it in one go.
#the difference between evaluate and execute is expressions need to be evaluated statements
#are executed. you will only need execute for if else and while, usually.  but you can include both
#in each Node subbed class
yacc.yacc(debug=0, write_tables=0)
filename = sys.argv[1]
## Open the file with read only permit
f = open(filename, "r")
lines = f.readlines()

data =""
for line in lines:
    if (len(line.strip()) == 0):
        continue
    else:
        data = data+line


compiledProgram = yacc.parse(data)

#compiledProgram.execute()
