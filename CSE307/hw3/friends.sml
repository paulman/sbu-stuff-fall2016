fun delete (item, list) = List.filter(fn x => x <> item) list

fun largest [] = 0
 | largest [x] = x
 | largest (x::xs) = Int.max(x, largest xs)

fun survey(ppl, intro, conf) =
    if ppl = 0 then 0
    else if intro = [] then 0
    else if conf = [] then 0
    else (largest(conf) + largest(delete(largest(conf), conf)));
