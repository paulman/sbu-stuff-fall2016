fun len(L) =
    if L = [] then 0
    else 1 + len(tl(L));

fun get_height(L, m, i) =
    if i = 0 then m
    else if hd(tl(L)) = 0 then (get_height(tl(L), Int.max(m, i+1), i+1))
    else    (get_height(tl(L), m, i-1));

fun list_sub(src, dst, i, j) = if src = [] then dst
    else if i = j then dst@[hd(src)]
    else list_sub(tl(src), dst@[hd(src)], i+1, j);

fun fbe(L, m, i) =
    if L = [] then m
    else if tl(L) = [] then (m)
    else if hd(L) = 0 then ( fbe(tl(L), m+1, i+1))
    else if hd(L) = 1 andalso (i - 1) = 0 then (m)
    else ( fbe(tl(L), m+1, i-1));


fun delete (_,   nil) = nil
    | delete (0, _::xs) = xs
    | delete (i, x::xs) = x::delete(i-1,xs);


fun trim_box(L) =
    if L = [] then []
    else tl( delete( fbe( L, 0, 0 ), L ) );

fun parse_brackets [] = 0
    | parse_brackets x =
        let
            val width = fbe(x, 0, 0)
            val height = get_height(x, 1, 1)
        in
            width * height
        end

fun pred_len(L) =
    if (hd(L) = 0) andalso (hd(tl(L)) = 1) then 2
    else 1;

(*start_parse_white(L, area) =
    if L = [] then area
    else*)
fun con_ones(L, i, j) =
    if L = [] then 0
    else if j < i then con_ones(tl(L), i, j+1)
    else if (j = i) andalso (hd(L) = 0) then 0
    else if hd(L) = 1 then 1 + con_ones(tl(L), i, j)
    else 0;

fun start_parse(L, i, area, test) =
    if L = [] then area
    else if ((i mod 2) = 0) then  (*on black, sub white*)
        (start_parse(trim_box(L), i+1+con_ones(test, i+1, 0), area + parse_brackets((L)), test))
    else                          (*on white, add black*)
        (start_parse(trim_box(L), i+1+con_ones(test, i+1, 0), area - parse_brackets((L)),test));

fun remove_box(L, L2, i, x) =
    if L = [] then (L2)
    else if i >= x then remove_box(tl(L), L2@[hd(L)], i+1, x)
    else remove_box(tl(L), L2, i+1, x);

fun start_brackets(L, area) =
    if L = [] then area
    (* else get area for box and add it to current area *)
    else (start_brackets(remove_box(L, [], 0, fbe(L, 0, 0)+1) , area + start_parse(list_sub(L, [], 0, fbe(L, 0, 0)), 0, 0, L)));

fun brackets(L) =
    if L = [] then 0
    else start_brackets(L, 0);
