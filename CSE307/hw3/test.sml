(*fun printList ints = app (fn i => print(Int.toString i ^" ")) ints;

fun list_sub(src, dst, i, j) = if src = [] then dst
    else if i = j then dst@[hd(src)]
    else list_sub(tl(src), dst@[hd(src)], i+1, j);

fun fbe(L, m, i) =
    (*(print ("List: " ^ L ^ "\n"));*)
    if L = [] then m
    else if tl(L) = [] then (m)
    else if hd(L) = 0 then ( fbe(tl(L), m+1, i+1))
    else if hd(L) = 1 andalso (i - 1) = 0 then (m)
    else ( fbe(tl(L), m+1, i-1));

fun show_parts(L, L2) =
    if L = [] then L2
    else (printList(L) ; print("   sh\n") ; L2);

fun remove_box(L, L2, i, x) =
    if L = [] then (printList(L2); print("   rm\n"); L2)
    else if i >= x then remove_box(tl(L), L2@[hd(L)], i+1, x)
    else remove_box(tl(L), L2, i+1, x);

fun test3(L) = hd(L);

fun test(L) =
    if L = [] then 0
    else test(show_parts(list_sub(L, [], 0, fbe(L, 0, 0)), remove_box(L, [], 0, fbe(L, 0, 0)+1)));*)

fun con_ones(L, i, j) =
    if L = [] then 0
    else if j < i then con_ones(tl(L), i, j+1)
    else if (j = i) andalso (hd(L) = 0) then 0
    else if hd(L) = 1 then 1 + con_ones(tl(L), i, j)
    else 0;

fun test4(L) =
    if L = [] then 0
    else con_ones(L, 3, 0);
(*
fun test2(L) =
    if L = [] then 0
    else ((val x = Int.max(0,2)) ; Int.max(x, 0));*)
(*OS.Process.exit(OS.Process.success);*)


(* End SML terminal
OS.Process.exit(OS.Process.success);
*)
fun printList ints = app (fn i => print(Int.toString i ^" ")) ints;

fun len(L) =
    if L = [] then 0
    else 1 + len(tl(L));

fun max_num(x, y) =
    if x > y then x
    else if x < y then y
    else x;

fun get_height(L, m, i) =
    if i = 0 then m
    else if hd(tl(L)) = 0 then (get_height(tl(L), Int.max(m, i+1), i+1))
    else    (get_height(tl(L), m, i-1));

fun list_sub(src, dst, i, j) = if src = [] then dst
    else if i = j then dst@[hd(src)]
    else list_sub(tl(src), dst@[hd(src)], i+1, j);

fun fbe(L, m, i) =
    if L = [] then m
    else if tl(L) = [] then (m)
    else if hd(L) = 0 then ( fbe(tl(L), m+1, i+1))
    else if hd(L) = 1 andalso (i - 1) = 0 then (m)
    else ( fbe(tl(L), m+1, i-1));


fun delete (_,   nil) = nil
    | delete (0, _::xs) = xs
    | delete (i, x::xs) = x::delete(i-1,xs);


fun trim_box(L) =
    if L = [] then []
    else tl( delete( fbe( L, 0, 0 ), L ) );

fun parse_brackets [] = 0
    | parse_brackets x =
        let
            val width = fbe(x, 0, 0)
            val height = get_height(x, 1, 1)
        in
            (print("list: "));
            (printList(x));
            (print ("\n"));
            width * height
        end

fun pred_len(L) =
    if (hd(L) = 0) andalso (hd(tl(L)) = 1) then 2
    else 1;

(*start_parse_white(L, area) =
    if L = [] then area
    else*)
fun con_ones(L, i, j) =
    if L = [] then 0
    else if j < i then con_ones(tl(L), i, j+1)
    else if (j = i) andalso (hd(L) = 0) then 0
    else if hd(L) = 1 then 1 + con_ones(tl(L), i, j)
    else 0;

fun start_parse(L, i, area, test) =
    if L = [] then area
    else if ((i mod 2) = 0) then  (*on black, sub white*)
        (printList(L);print("len: "^Int.toString(len(L))^"\ti: " ^ Int.toString(i+1+con_ones(test, i+1, 0)) ^ "\tarea: "^Int.toString(area) ^ "\tsubtotal +: " ^ Int.toString(area + parse_brackets((L))) ^ "\n\n");
        start_parse(trim_box(L), i+1+con_ones(test, i+1, 0), area + parse_brackets((L)), test))
    else                          (*on white, add black*)
        (printList(L);print("len: "^Int.toString(len(L))^"\ti: " ^ Int.toString(i+1+con_ones(test, i+1, 0)) ^ "\tarea: "^Int.toString(area) ^ "\tsubtotal -: " ^ Int.toString(area - parse_brackets((L))) ^ "\n\n");
        start_parse(trim_box(L), i+1+con_ones(test, i+1, 0), area - parse_brackets((L)),test));

fun remove_box(L, L2, i, x) =
    if L = [] then (printList(L2); print("   rm\n"); L2)
    else if i >= x then remove_box(tl(L), L2@[hd(L)], i+1, x)
    else remove_box(tl(L), L2, i+1, x);

fun start_brackets(L, area) =
    if L = [] then area
    (* else get area for box and add it to current area *)
    else (print("New box:\n" ^ "area:\t"^Int.toString(area)^"\n");
        start_brackets(remove_box(L, [], 0, fbe(L, 0, 0)+1) , area + start_parse(list_sub(L, [], 0, fbe(L, 0, 0)), 0, 0, L)));

fun brackets(L) =
    if L = [] then 0
    else start_brackets(L, 0);
























(*brackets([0,1]);
brackets([0,0,0,1,1,1]);
brackets([0,0,1,1,0,0,1,0,0,1,1,1]);*)
